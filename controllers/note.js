var Models = require('../models/index.js');
var shortid = require('shortid');

exports.NotesController  = (function (){

	return {
	
		saveNoteLinks 	: 	function (note, cbk){
 			try{
			 	var nte = Models.Note({
			 		title		: 	note.title,
    				shortUrl	: 	shortid.generate(),
 					dateCreated : 	Date.now(),
    				dateDeleted : 	null,
    				deleted 	: 	0,
			 	});

			   nte.save(function(err,nt){
			        for (var i = 0; i< note.links.length; i++){
			            var link = new Models.Link(note.links[i]);
			            link.idNote = nte._id;
			            link.shortUrl = shortid.generate();
						link.dateCreated = 	Date.now();
						nte.links.push(link);

			            link.save(function(err, lnk)
			            {
			                if(note.links.length == i){
								Models.Note.update({"_id": nte._id }, {$set: {"links": nte.links}}, function(err,data){
				        			cbk(null, data);
								});
			                }

			            });
			        }
				});
			}catch (e)
			{
				cbk(e,null);

			}
		},
		getAll			:	function (params, cbk){

			
			Models.Note.find(
				{ "_id" : { "$nin": params.idsExclude } })
    			.sort( params.sort )
				.skip( params.skip )
				.limit( params.limit )
				.exec (function (error, data){


				});
		},

	
	}
})();