var restify = require('restify');
var routes = require('./routes.js');
var mongoose = require('mongoose');
var config = require('./config.js');

var server = restify.createServer({
  name:   config.name,
});

mongoose.connect(config.mongodb);

server.use(restify.CORS());
server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());

//api.links.ly/v.1/note/
server.post(config.version + '/note',routes.Notes.createNoteWithLinks);


// server.get(config.version + '/notes',routes.Notes.getAll);
// server.get(config.version + '/notes/:userName',routes.Notes.getByUserName);


server.listen(3000);