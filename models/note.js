var mongoose = require('mongoose');

var ObjectId = mongoose.Schema.ObjectId;

var NoteModel = mongoose.model('Note', new mongoose.Schema(
{
   // _id: { type : ObjectId},
    title: String,
    shortUrl: String,
    links : 
    	{ 
    		type : [ObjectId], ref: 'Link'
    	},
    userCreated : { 
    		type : ObjectId, ref: 'User'
    	},
    dateCreated : { type: Date, default:  Date.now },
    dateDeleted :  { type: Date },
    deleted:	{ type: Boolean, default: 0 },
}));



exports.Note = NoteModel;