
var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.ObjectId;

var UserModel =  mongoose.model('User', new mongoose.Schema(
{
    user  	: String,
    name	: String,
    password	: String,
    dateCreated : { type: Date, default:  Date.now },
    dateDeleted :  { type: Date },
    deleted:	{ type: Boolean, default: 0 },
}));


// var user = new Models.User.User({
//     user : 'bruno',
//     name : 'bruno',
//     password : '',
//     dateCreated : Date.now(),
// });
// user.save(function(err,nt){
//     console.log(err)

// });
exports.User = UserModel;