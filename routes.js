var NotesController = require('./controllers/note.js').NotesController;


	exports.Notes	= (function (){
		return {
			createNoteWithLinks : function (req, res, next){
					var nt = req.params;
					NotesController.saveNoteLinks(nt, function(err, data){
						if(err){
							res.end(JSON.stringify(err));
						}else{
							res.end(JSON.stringify(data));
						}
					});
					return next();
				},

			// getAll : function (req, res, next){
			// 		var nt = req.params;
			// 		NotesController.saveNoteLinks(nt, function(err, data){
			// 			if(err){
			// 				res.end(JSON.stringify(err));
			// 			}else{
			// 				res.end(JSON.stringify(data));
			// 			}
			// 		});
			// 		return next();
			// 	},
		}
	})();

	// exports.Links	= (function (){
	// 	return {

	// 		getAll : function (req, res, next){
	// 				var nt = req.params;
	// 				NotesController.saveNoteLinks(nt, function(err, data){
	// 					if(err){
	// 						res.end(JSON.stringify(err));
	// 					}else{
	// 						res.end(JSON.stringify(data));
	// 					}
	// 				});
	// 				return next();
	// 			},
	// 	}
	// })();
